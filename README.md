#DiscordBots

---
Bot framework based on DSharpPlus that aims to provide basic functionality for The Arcane Brony's bots.

#Scripts

---
install.sh: install as a systemd service (only meant for deployment, not development)

monitor.sh: show output and resource usage in a tmux session (DO NOT USE)

import_prod_db.sh: clone production db locally, assuming you have access to my server