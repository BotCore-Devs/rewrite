using System.Diagnostics;
using System.Net;
using BotCore.DataModel;
using BotCore.Util.Systemd;
using BotCore.Web;
using Microsoft.AspNetCore.HttpLogging;
using Microsoft.EntityFrameworkCore;
using Sentry;
using Config = BotCore.Web.Config;
using Timer = System.Timers.Timer;

var webCfg = Config.Read();
webCfg.Save();

var envname = webCfg.SentryEnvironment;
if (envname.Length < 1)
{
    Console.WriteLine("Environment name not set! Using hostname, to change this, set in Config.json!");
    envname = Environment.MachineName;
}

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddHttpLogging(o => o.LoggingFields = HttpLoggingFields.All);
builder.Services.AddLogging(o => o.AddSystemdConsole());
builder.WebHost.UseSentry(o =>
{
    o.Dsn = "https://ded8147d51084bfc808c4d73b7b583b1@sentry.thearcanebrony.net/3";
    o.TracesSampleRate = 1.0;
    o.AttachStacktrace = true;
    o.MaxQueueItems = int.MaxValue;
    o.StackTraceMode = StackTraceMode.Original;
    o.Environment = envname;
});
var cfg = DbConfig.Read();
cfg.Save();
using (var db = new Db(new DbContextOptionsBuilder<Db>()
           .UseNpgsql(
               $"Host={cfg.Host};Database={cfg.Database};Username={cfg.Username};Password={cfg.Password};Port={cfg.Port};Include Error Detail=true")
           .LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().Options))
{
    db.Database.Migrate();
    db.SaveChanges();
    foreach (var dbBot in db.Bots) Console.WriteLine($"Listening on {dbBot.Name}.localhost -> {dbBot.FriendlyName}");
}

var avatarQueueWorker = new Timer(new TimeSpan(0, 0, 5).TotalMilliseconds);
avatarQueueWorker.Elapsed += (_, _) =>
{
    if (DataStore.AvatarsToUpdate.Count <= 0) return;
    using var db = new Db(new DbContextOptionsBuilder<Db>()
        .UseNpgsql(
            $"Host={cfg.Host};Database={cfg.Database};Username={cfg.Username};Password={cfg.Password};Port={cfg.Port};Include Error Detail=true")
        .LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().Options);
    foreach (var id in DataStore.AvatarsToUpdate.ToArray())
    {
        db.GlobalUsers.First(x => x.DiscordUserId == id.ToString()).AvatarUrl = "";
        DataStore.AvatarsToUpdate.Remove(id);
    }

    db.SaveChanges();
};
avatarQueueWorker.Start();
var faviconDownloader = new Timer(new TimeSpan(0, 0, 5).TotalMilliseconds);
faviconDownloader.Elapsed += (_, _) =>
{
    faviconDownloader.Interval = new TimeSpan(0, 15, 0).TotalMilliseconds;
    if (!Directory.Exists("Resources/RunData")) Directory.CreateDirectory("Resources/RunData");
    using WebClient wc = new();
    using var db = new Db(new DbContextOptionsBuilder<Db>()
        .UseNpgsql(
            $"Host={cfg.Host};Database={cfg.Database};Username={cfg.Username};Password={cfg.Password};Port={cfg.Port};Include Error Detail=true")
        .LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().Options);
    foreach (var bot in db.Bots.Where(x => x.AvatarUrl.Length >= 2))
    {
        if (File.Exists("Resources/RunData/" + bot.Name + ".png")) continue;
        Console.WriteLine($"{bot.AvatarUrl.Replace(".webp", ".png")} -> Resources/RunData/{bot.Name}.png");
        wc.DownloadFile(bot.AvatarUrl.Replace(".webp", ".png"), "Resources/RunData/" + bot.Name + ".png");
    }

    db.SaveChanges();
};
faviconDownloader.Start();
builder.Services.AddDbContextPool<Db>(optionsBuilder =>
{
    optionsBuilder
        .UseNpgsql(
            $"Host={cfg.Host};Database={cfg.Database};Username={cfg.Username};Password={cfg.Password};Port={cfg.Port}")
        .LogTo(str => Debug.WriteLine(str), LogLevel.Information).EnableSensitiveDataLogging();
});
var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseWebSockets();

app.UseRouting();
app.UseSentryTracing();
app.UseAuthorization();

app.UseEndpoints(endpoints => { endpoints.MapControllerRoute("default", "{controller=Home}/{action=Index}/{id?}"); });
app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
app.Use((context, next) =>
{
    context.Response.Headers["Content-Type"] += "; charset=utf-8";
    return next.Invoke();
});
app.MapControllers();

SentrySdk.CaptureMessage("BotCore.Web.Legacy started!");
Systemd.NotifyReady();
app.Run();