window.onload = () => {
    const request = new Request("/api/thisbot");
    fetch(request)
        .then(response => response.json())
        .then(bot => {
            console.log(bot);
            $("center").append(`<h1>${bot.friendlyName}</h1>`);
            $("center").append(`<p><i>${bot.onlineMemberCount}</i> of <i>${bot.memberCount}</i> members online in <i>${bot.serverCount}</i> servers</p>`);
            $("#footer").append(`</div><img src='https://img.shields.io/badge/BotCore-${bot.coreVer}-green'>`);
        });
};