var botid = 0;
window.onload = () => {
    fetch(new Request("/api/thisbot"))
        .then(response => response.json())
        .then(bot => {
            console.log(bot);
            botid = bot.botId;
            $("#footer").append(`</div><img src='https://img.shields.io/badge/BotCore-${bot.coreVer}-green'>`);
            $("center").append(`<h1>Leaderboards for ${bot.friendlyName}</h1>`);
            fetch(new Request(`/api/servers?bid=${botid}`))
                .then(response => response.json())
                .then(list => {
                    list.sort((a, b) => {
                        return a.memberCount < b.memberCount ? 1 : -1;
                    });
                    console.log(list);
                    list.forEach((a) => {
                        if (a.memberCount >= 1)
                            $("#content").append(`<div class="clickable card" onclick="document.location='/leaderboards/${a.discordServerId}'">
		<img class='icon' loading='lazy' src='${a.iconUrl}'>
		<a class="name">${a.name}</a><br>
		<a class="description">${a.memberCount} members</a>
	</div>`);
                    });
                });
        });
};