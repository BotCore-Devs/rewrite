using Microsoft.AspNetCore.Mvc;

namespace BotCore.Web.Frontend.Controllers;

public class LeaderboardsController : Controller
{
    public IActionResult Index()
    {
        return View();
    }

    [HttpGet("/leaderboards/{id}")]
    public IActionResult Guild(string id)
    {
        Console.WriteLine(id);
        ViewData["id"] = id;
        return View();
    }
    [HttpGet("/leaderboards/{guildid}/{userid}")]
    public IActionResult User(string guildid, string userid)
    {
        ViewData["id"] = guildid;
        ViewData["uid"] = userid;
        return View();
    }
}