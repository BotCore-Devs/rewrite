using ArcaneLibs;

namespace BotCore.Web.Frontend;

public class Config : SaveableObject<Config>
{
    public string SentryEnvironment { get; } = "";
}