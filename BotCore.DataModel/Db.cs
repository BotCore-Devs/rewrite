﻿using System.ComponentModel.DataAnnotations.Schema;
using ArcaneLibs.Logging;
using ArcaneLibs.Logging.LogEndpoints;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace BotCore.DataModel;

public class Db : DbContext
{
    [NotMapped] private static LogManager? dbLogger;

    [NotMapped] private static readonly List<Db> contexts = new();

    //props
    public bool InUse = true;

    //db
    public Db(DbContextOptions<Db> options)
        : base(options)
    {
        Config = new(this);
    }

    [NotMapped]
    public Config Config;
    public DbSet<Bot> Bots { get; set; } = null!;
    public DbSet<DServerUser> DServerUsers { get; set; } = null!;
    public DbSet<GlobalUser> GlobalUsers { get; set; } = null!;
    public DbSet<Quote> Quotes { get; set; } = null!;
    public DbSet<Server> Servers { get; set; } = null!;
    public DbSet<Warning> Warnings { get; set; } = null!;
    public DbSet<ConfigValue> ConfigValues { get; set; } = null!;
    public DbSet<SlowmodeConfig> SlowmodeConfigs { get; set; } = null!;
    public DbSet<SlowmodeData> SlowmodeDatas { get; set; } = null!;
    public DbSet<AntiScamLog> AntiScamLogs { get; set; } = null!;

    //         protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    //         {
    //             if (!optionsBuilder.IsConfigured) {
    //                 var cfg = DbConfig.Read();
    //                 cfg.Save();
    // #warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
    //                 optionsBuilder.UseLazyLoadingProxies().UseNpgsql($"Host={cfg.Host};Database={cfg.Database};Username={cfg.Username};Password={cfg.Password};Port={cfg.Port}").LogTo((str)=>Debug.WriteLine(str), LogLevel.Information).EnableSensitiveDataLogging();
    //             }
    //         }

    // protected override void OnModelCreating(ModelBuilder modelBuilder)
    // {
    // OnModelCreatingPartial(modelBuilder);
    // }

    // private void OnModelCreatingPartial(ModelBuilder modelBuilder)
    // {
    //     throw new NotImplementedException();
    // }

    //overrides
    public override void Dispose()
    {
        InUse = false;
        base.Dispose();
    }

    public override ValueTask DisposeAsync()
    {
        InUse = false;
        return base.DisposeAsync();
    }

    internal static LogManager GetDbModelLogger()
    {
        if (dbLogger == null)
        {
            dbLogger = new LogManager
            {
                Prefix = "[DbModel] ",
                LogTime = true
            };
            dbLogger.AddEndpoint(new DebugEndpoint());
        }

        return dbLogger;
    }

    public static string GetConnStr()
    {
        var cfg = DbConfig.Read();
        cfg.Save();
        return
            $"Host={cfg.Host};Database={cfg.Database};Username={cfg.Username};Password={cfg.Password};Port={cfg.Port};Include Error Detail=true";
    }
    public static Db GetNewPostgres()
    {
        GetDbModelLogger().Log("Instantiating new DB context: Postgres");
        var cfg = DbConfig.Read();
        cfg.Save();
        var dbcob = new DbContextOptionsBuilder<Db>()
            .UseNpgsql(
                $"Host={cfg.Host};Database={cfg.Database};Username={cfg.Username};Password={cfg.Password};Port={cfg.Port};Include Error Detail=true")
            .LogTo(log, LogLevel.Information).EnableSensitiveDataLogging();
        var db = new Db(dbcob.Options);
        contexts.Add(db);
        db.Database.Migrate();
        db.SaveChanges();
        return db;
    }

    public static Db GetPostgres()
    {
        GetDbModelLogger().Log("Instantiating new DB context: Postgres");
        var cfg = DbConfig.Read();
        cfg.Save();
        if (contexts.Any(x => !x.ChangeTracker.HasChanges())) return contexts.First(x => !x.ChangeTracker.HasChanges());
        var db = GetNewPostgres();
        contexts.Add(db);
        return db;
    }
    // public static Db GetSqlite()
    // {
    // GetDbModelLogger().Log("Instantiating new DB context: Sqlite");
    // var cfg = DbConfig.Read();
    // cfg.Save();
    // return new Db(new DbContextOptionsBuilder<Db>().UseSqlite($"Data Source={cfg.Database}.db;Version=3;")
    // .LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().Options);
    // }
    // public static Db GetInMemoryDb()
    // {
    // GetDbModelLogger().Log("Instantiating new DB context: InMemory");
    // var cfg = DbConfig.Read();
    // cfg.Save();
    // return new Db(new DbContextOptionsBuilder<Db>().UseInMemoryDatabase("InMemoryDb")
    // .LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().Options);
    // }

    private static void log(string text)
    {
        GetDbModelLogger().Log(text);
    }
}