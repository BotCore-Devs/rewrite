﻿// See https://aka.ms/new-console-template for more information

using BotCore.Classes;
using BotCore.Runner.Bots;
using BotCore.Util;
using Bots.BinSh.Commands;
using Bots.FosscordBot.Commands;

Console.WriteLine("Hello, World!");

if (args.Length == 0)
{
    Console.WriteLine("Missing argument: bot name");
}
else
{
    Console.WriteLine($"BotCore Runner - {VersionUtils.GetLongVersion()} - Starting bot `{args[0]}`");
    switch (args[0])
    {
        case "spacebot":
            BotWithCommands.Run(args[0], typeof(Github), typeof(ImportToFosscord));
            break;
        case "binsh":
            BotWithCommands.Run(args[0], typeof(Roles));
            break;
        case "sugarcanebot":
            BotWithCommands.Run(args[0]);
            break;
        default:
            GenericBot.Run(args[0]);
            break;
    }
}

//never exit
Thread.Sleep(int.MaxValue);