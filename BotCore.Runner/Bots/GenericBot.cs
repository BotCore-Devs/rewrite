using BotCore.DataModel;
using BotCore.DbExtras;
using BotCore.Util.DbExtensions;
using Monitor = BotCore.Monitoring.Monitor;

namespace BotCore.Runner.Bots;

public class GenericBot
{
    public static BotImplementation Run(string name)
    {
        var db = Db.GetPostgres();
        var bot = db.GetBot(name);
        var bi = new BotImplementation(db, bot);
        Monitor.StartMonitoring(bi);
        return bi;
    }
}