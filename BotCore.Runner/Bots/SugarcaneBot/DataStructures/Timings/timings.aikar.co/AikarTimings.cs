using System.Net;
using Newtonsoft.Json;

namespace Bots.SugarcaneBot.DataStructures.Timings.timings.aikar.co;

public class AikarTimings
{
    public static AikarTimingsRoot FromUrl(string url)
    {
        return JsonConvert.DeserializeObject<AikarTimingsRoot>(
            new WebClient().DownloadString(url.Replace("/?", "/data.php?")));
    }
}