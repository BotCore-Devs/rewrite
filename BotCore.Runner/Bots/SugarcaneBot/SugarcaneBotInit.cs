using System.Net;
using Bots.SugarcaneBot.DataStructures.Timings.timings.aikar.co;
using Newtonsoft.Json;

namespace Bots.SugarcaneBot;

public class SugarcaneBotInit
{
    public void Init()
    {
        var aikartest = JsonConvert.DeserializeObject<AikarTimingsRoot>(File.ReadAllText("aikar.json"));
        Console.WriteLine(JsonConvert.SerializeObject(aikartest.TimingsMaster.Configs, new JsonSerializerSettings
        {
            Formatting = Formatting.Indented,
            ContractResolver = new LongNameContractResolver()
        }));
        aikartest.GetRecommendations();
//Environment.Exit(0);

        if (!Directory.Exists("timings")) Directory.CreateDirectory("timings");
        foreach (var url in File.ReadAllLines("timings.txt"))
        {
            var id = url.Split("id=")[1].Split("#")[0];
            if (!File.Exists($"timings/{id}.json"))
            {
                Console.WriteLine("Downloading: " + url);
                new WebClient().DownloadFile(url.Replace("/?", "/data.php?"), $"timings/{id}.json");
            }

            var timing = JsonConvert.DeserializeObject<AikarTimingsRoot>(File.ReadAllText($"timings/{id}.json"));

            Console.WriteLine($"ID: {timing.Id}");
            Console.WriteLine($"Version: {timing.TimingsMaster.Version}");
            foreach (var rec in timing.GetRecommendations()) Console.WriteLine(rec.title + ": " + rec.description);
        }

        Environment.Exit(0);
    }
}