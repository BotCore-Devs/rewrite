using BotCore.Commands;
using BotCore.Commands.Commands;
using BotCore.DbExtras;
using Bots.BinSh.DbModel;
using DSharpPlus;
using DSharpPlus.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace Bots.BinSh.Commands;

public class Roles : CommandGroup
{
    public static RoleDbConfig rcfg = RoleDbConfig.Read();

    public static RoleDb role_db = new(new DbContextOptionsBuilder<RoleDb>()
        .UseNpgsql(
            $"Host={rcfg.Host};Database={rcfg.Database};Username={rcfg.Username};Password={rcfg.Password};Port={rcfg.Port};Include Error Detail=true")
        .LogTo(Console.WriteLine, LogLevel.Information).EnableSensitiveDataLogging().Options);

    //cr, create missing roles
    public static Command CreateRolesCommand(BotImplementation bot)
    {
        return new()
        {
            Name = "cr",
            Category = "Roles",
            Description = "Create missing roles",
            action = async (me, ce) =>
            {
                var roleChannels = new ulong[] {924698038814011434};
                List<DiscordThreadChannel> rolech = new();
                var g = await me.Bot.DiscordClient.GetGuildAsync(924696406776745994);
                List<DiscordThreadChannel> list = new();
                var latr = await g.ListActiveThreadsAsync();
                list.AddRange(latr.Threads);
                while (latr.HasMore)
                {
                    latr = await g.ListActiveThreadsAsync();
                    list.AddRange(latr.Threads);
                }

                var count = 0;
                Console.WriteLine($"Found {count = list.Count} active threads");
                foreach (var ch in await g.GetChannelsAsync())
                    try
                    {
                        if (!ch.IsThread && !ch.IsCategory && ch.Type != ChannelType.Voice)
                        {
                            latr = await ch.ListPrivateArchivedThreadsAsync();
                            list.AddRange(latr.Threads);
                            while (latr.HasMore)
                            {
                                latr = await ch.ListPrivateArchivedThreadsAsync();
                                list.AddRange(latr.Threads);
                            }

                            latr = await ch.ListPublicArchivedThreadsAsync();
                            list.AddRange(latr.Threads);
                            while (latr.HasMore)
                            {
                                latr = await ch.ListPublicArchivedThreadsAsync();
                                list.AddRange(latr.Threads);
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine($"Exception in {ch}:\n{e}");
                    }

                Console.WriteLine($"Found {list.Count - count} archived threads");
                rolech = list.Where(x => roleChannels.Contains(x.ParentId ?? 0)).ToList();
                Console.WriteLine(string.Join("\n", "", 0, rolech.Select(x => x.ToString()).ToArray()));
                Console.WriteLine("Scanning for roles");
                List<string> warnings = new();
                Console.WriteLine(g.Roles.Count);
                foreach (var ch in rolech)
                foreach (var msg in await ch.GetMessagesAsync())
                {
                    Console.WriteLine($"{msg.Content}: {g.Roles.Any(x => x.Value.Name == msg.Content)}");
                    if (!g.Roles.Any(x => x.Value.Name == msg.Content))
                        warnings.Add($"Could not find a related role for message: {msg.Content}");
                    else if (!role_db.Roles.Any(x => x.DiscordServerId == ch.GuildId && x.DiscordMessageId == msg.Id))
                        role_db.Roles.Add(new RoleEntry
                        {
                            DiscordMessageId = msg.Id,
                            DiscordServerId = ch.GuildId.Value,
                            DiscordRoleId = g.Roles.First(x => x.Value.Name == msg.Content).Key
                        });
                }

                role_db.SaveChanges();
                Console.WriteLine(string.Join("\n", warnings));
                foreach (var rch in rolech)
                {
                    var gg = await me.Bot.DiscordClient.GetGuildAsync(rch.GuildId.Value);
                    foreach (var msg in await rch.GetMessagesAsync())
                        if (!gg.Roles.Any(x => x.Value.Name == msg.Content) && msg.Content.Length <= 32)
                        {
                            var r = await gg.CreateRoleAsync(msg.Content, Permissions.None,
                                reason: "missing role from role list");
                            role_db.Roles.Add(new RoleEntry
                            {
                                DiscordMessageId = msg.Id,
                                DiscordServerId = rch.GuildId.Value,
                                DiscordRoleId = r.Id
                            });
                        }
                        else if (!role_db.Roles.Any(x =>
                                     x.DiscordServerId == rch.GuildId && x.DiscordMessageId == msg.Id) &&
                                 gg.Roles.Any(x => x.Value.Name == msg.Content))
                        {
                            role_db.Roles.Add(new RoleEntry
                            {
                                DiscordMessageId = msg.Id,
                                DiscordServerId = rch.GuildId.Value,
                                DiscordRoleId = gg.Roles.First(x => x.Value.Name == msg.Content).Key
                            });
                        }
                }

                role_db.SaveChanges();
                return null;
            },
            CanRun = async (environment, cmdenvironment) => environment.Message.Author.Id == 84022289024159744
        };
    }
}