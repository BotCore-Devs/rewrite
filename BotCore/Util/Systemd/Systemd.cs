using System.Runtime.InteropServices;
using BotCore.DataModel;

namespace BotCore.Util.Systemd;

public static class Systemd
{
    [DllImport("libsystemd.so.0")]
    private static extern int sd_notify(int unset_environment, string state);

    public static void NotifyReady()
    {
        try
        {
            sd_notify(0, "READY=1");
        }
        catch (Exception ex)
        {
            // do something because we're really boned.
            Console.WriteLine("Failed to SD_NOTIFY");
        }
    }

    public static void BailIfDisabled(Bot bot)
    {
        Console.WriteLine($"Bot \"{bot.Name}\" not enabled in database!");
        if (File.Exists($"/etc/systemd/system/botcore.bot.{bot.Name}.service"))
        {
            File.Delete($"/etc/systemd/system/botcore.bot.{bot.Name}.service");
            ArcaneLibs.Util.RunCommandSync("systemctl", "daemon-reload");
            ArcaneLibs.Util.RunCommandSync("systemctl", $"disable --now botcore.bot.{bot.Name}");
        }

        Console.WriteLine($"Bot not enabled in database: {bot.Name}! Deleted systemd unit if it existed!");
        Environment.Exit(0);
    }
}