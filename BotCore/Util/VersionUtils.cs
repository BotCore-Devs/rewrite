namespace BotCore.Util;

public static class VersionUtils
{
    //stateful tracking
    private static DateTime _lastFetch = DateTime.UnixEpoch;
    private static (int Behind, int Ahead, int Modified, int Staged) _gitStatus = (0,0,0,0);

    public static string VersionTag = "v4.0.0";

    public static string GetVersion()
    {
        string ver = ArcaneLibs.Util.GetCommandOutputSync("git", "rev-parse --short HEAD");
        if (GetGitStatus().Modified != 0) ver += "-unstaged";
        return ver.Replace("\n", "");
    }

    public static int GetCommitsSinceTag(string tag)
    {
        return int.Parse(ArcaneLibs.Util.GetCommandOutputSync("git", $"rev-list {tag}..HEAD --count"));
    }

    public static (int Behind, int Ahead, int Modified, int Staged) GetGitStatus()
    {
        List<string> mfiles = new();
        DoFetch();
        _gitStatus.Behind = int.Parse(ArcaneLibs.Util.GetCommandOutputSync("git", "rev-list --count HEAD..@{u}"));
        _gitStatus.Ahead = int.Parse(ArcaneLibs.Util.GetCommandOutputSync("git", "rev-list --count @{u}..HEAD"));
        mfiles.AddRange(ArcaneLibs.Util.GetCommandOutputSync("git", "diff --name-only --cached").Split("\n"));
        _gitStatus.Modified = ArcaneLibs.Util.GetCommandOutputSync("git", "diff --name-only").Split("\n")
            .Count(x => !mfiles.Contains(x));
        _gitStatus.Staged = ArcaneLibs.Util.GetCommandOutputSync("git", "diff --name-only --cached").Count(x => x == '\n');
        
        return _gitStatus;
    }

    public static string GetLongVersion()
    {
        return $"v4.0.0.{GetCommitsSinceTag(VersionTag)}+" + GetVersion();
    }

    public static void DoFetch()
    {
        if (!ShouldUpdate()) return;
        _lastFetch = DateTime.Now;
        ArcaneLibs.Util.RunCommandSync("git", "fetch");
    }

    private static bool ShouldUpdate()
    {
        return DateTime.Now - _lastFetch >= TimeSpan.FromHours(2);
    }
}