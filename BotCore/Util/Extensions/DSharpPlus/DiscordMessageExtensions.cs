using DSharpPlus.Entities;

namespace BotCore.Util.Extensions.DSharpPlus;

public static class DiscordMessageExtensions
{
    public static Task<DiscordMember> GetAuthorAsMemberAsync(this DiscordMessage message)
    {
        if(message.Author is DiscordMember)
            return Task.FromResult((DiscordMember) message.Author);

        return message.Channel.Guild.GetMemberAsync(message.Author.Id);
    }
}