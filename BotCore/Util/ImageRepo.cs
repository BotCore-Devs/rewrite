using System.IO.Compression;
using System.Net;

namespace BotCore.Util;

public static class ImageRepo
{
    private static readonly Dictionary<string, string[]> RepoContentCache = new();
    private static readonly Random Rnd = new();
    private static string ImageRepoRoot => BotEnvironment.SolutionDir + "/images/";

    public static void EnsureExists()
    {
        //Download floof
        if (!Directory.Exists(ImageRepoRoot + "floof"))
        {
            Directory.CreateDirectory(ImageRepoRoot + "floof");
            if (File.Exists("floof.zip")) File.Delete("floof.zip");
            Console.WriteLine("Downloading floof.zip...");
            new WebClient().DownloadFile("https://thearcanebrony.net/floof.zip", "floof.zip");
            ZipFile.ExtractToDirectory("floof.zip", ImageRepoRoot + "floof");
        }

        //Download headpats
        if (!Directory.Exists(ImageRepoRoot + "headpats"))
        {
            Directory.CreateDirectory(BotEnvironment.SolutionDir + "headpats");
            if (File.Exists("headpats.zip")) File.Delete("headpats.zip");
            Console.WriteLine("Downloading headpats.zip...");
            new WebClient().DownloadFile("https://thearcanebrony.net/headpats.zip", "headpats.zip");
            ZipFile.ExtractToDirectory("headpats.zip", ImageRepoRoot + "headpats");
        }

        //Download spray
        if (!File.Exists(ImageRepoRoot + "spray.jpg"))
            new WebClient().DownloadFile("https://thearcanebrony.net/spray.jpg", ImageRepoRoot + "spray.jpg");
    }

    //Get random filename in directory, by "repo" name
    //File lists are cached upon first access in case of use with slow file systems (eg. s3 buckets over s3fs)
    public static string GetRandom(string repo)
    {
        if (!RepoContentCache.ContainsKey(repo)) RepoContentCache.Add(repo, Directory.GetFiles(ImageRepoRoot + repo));
        return RepoContentCache[repo][Rnd.Next(RepoContentCache[repo].Length)];
    }

    public static string GetImage(string filename)
    {
        return ImageRepoRoot + filename;
    }
}