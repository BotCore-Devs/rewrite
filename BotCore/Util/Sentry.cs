using Sentry;

namespace BotCore.Util;

public static class Sentry
{
    public static void InitSentry(string name)
    {
        SentrySdk.Init(o =>
        {
            o.Dsn = "https://7d6c71abd3634257ab906d5956033724@sentry.thearcanebrony.net/10";
            o.Debug = false;
            o.TracesSampleRate = 1.0;
            o.Environment = $"{Environment.MachineName}_{name}";
            o.Release = VersionUtils.GetVersion();
        });
    }

    public static ITransaction StartTransaction(string name)
    {
        var transaction = SentrySdk.StartTransaction(name, name);
        SentrySdk.ConfigureScope(scope => scope.Transaction = transaction);
        return transaction;
    }
}