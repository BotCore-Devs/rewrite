﻿//TODO: refactor

using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;
using BotCore.Classes;
using BotCore.Util.Audio;
using DSharpPlus.VoiceNext;

namespace BotCore.Util;

internal static class ServerMusicTracking
{
    private static readonly Dictionary<ulong, ServerMusicPlayer> _players = new();

    public static ServerMusicPlayer GetPlayer(ulong guildId)
    {
        var player = new ServerMusicPlayer();
        if (!_players.ContainsKey(guildId)) _players.Add(guildId, player);
        else player = _players[guildId];
        return player;
    }
}

internal class ServerMusicPlayer
{
    public bool IsPlaying = false;
    public ServerMusicPlayerQueue Queue = new();
    public VoiceNextConnection Vnc;
    public VoiceTransmitSink vts;
    MusicItem currentlyPlaying;
    
    // public async void Play(CommandEnvironment ce)
    // {
    //     var vne = me.Bot.DiscordClient.GetVoiceNext();
    //     if (Vnc == null) await me.Channel.SendMessageAsync("Not connected?");
    //     if (!IsPlaying)
    //     {
    //         //VoiceNextConnection vnc;
    //         if ((await me.Channel.Guild.GetMemberAsync(me.Message.Author.Id)).VoiceState == null)
    //         {
    //             await me.Channel.SendMessageAsync("You must be connected to a voice channel!");
    //         }
    //         else if(vne.GetConnection(me.Channel.Guild) == null)
    //         {
    //             Vnc = await vne.ConnectAsync((await me.Channel.Guild.GetMemberAsync(me.Message.Author.Id)).VoiceState.Channel);
    //             await me.Channel.SendMessageAsync("Connected!");
    //         }
    //
    //         //string file = ce.E.Message.Content.Replace(me.Server.Prefix + "play ", "");
    //         if (vne.GetConnection(me.Channel.Guild) == null)
    //         {
    //             await me.Channel.SendMessageAsync("Not connected in this server!");
    //         }
    //         else
    //         {
    //             MusicItem musicItem;
    //             while ((musicItem = Queue.Next()) != null)
    //             {
    //                 Vnc = vne.GetConnection(me.Channel.Guild);
    //                 while (Vnc.IsPlaying) Thread.Sleep(100);
    //                 //if (!Directory.Exists("Music")) Directory.CreateDirectory("Music");
    //                 //if (!Directory.Exists("Music/" + ce.E.Guild.Id)) Directory.CreateDirectory("Music/" + ce.E.Guild.Id);
    //                 Vnc = vne.GetConnection(me.Channel.Guild);
    //                 var txStream = Vnc.GetTransmitSink();
    //                 txStream.VolumeModifier = 0.75;
    //
    //                 await me.Channel.SendMessageAsync("Downloading!");
    //                 var ytdlpsi = new ProcessStartInfo
    //                 {
    //                     FileName = "youtube-dl",
    //                     Arguments =
    //                         $@"-ci -f best ""{musicItem.Url}"" -o - --quiet", //Music/{ce.E.Guild.Id}/{Directory.GetFiles($"Music/{ce.E.Guild.Id}").Length}",
    //                     RedirectStandardOutput = true,
    //                     UseShellExecute = false
    //                 };
    //                 var ytdl = Process.Start(ytdlpsi);
    //                 //AudioDecoder.GetPCM(ytdl.StandardOutput.BaseStream);
    //                 var psi = new ProcessStartInfo
    //                 {
    //                     FileName = "ffmpeg",
    //                     Arguments = @"-i - -ar 48000 -f wav -", //Music/{ce.E.Guild.Id}/0
    //                     RedirectStandardOutput = true,
    //                     UseShellExecute = false,
    //                     RedirectStandardInput = true
    //                 };
    //                 var ffmpeg = Process.Start(psi);
    //
    //                 await me.Channel.SendMessageAsync("Preparing!");
    //                 if (ytdl != null)
    //                     if (ffmpeg != null)
    //                         new Thread(async () =>
    //                         {
    //                             while (!ytdl.HasExited)
    //                             {
    //                                 await ytdl.StandardOutput.BaseStream.CopyToAsync(ffmpeg.StandardInput.BaseStream);
    //                             }
    //                         }).Start();
    //                     else Console.WriteLine("ffmpeg was null");
    //                 else Console.WriteLine("ytdl was null");
    //                 if (ffmpeg != null)
    //                 {
    //                     var ffout = ffmpeg.StandardOutput.BaseStream;
    //
    //                     await me.Channel.SendMessageAsync($"Playing *{musicItem.Name}* (<{musicItem.Url}>)!");
    //                     new Thread(async () =>
    //                     {
    //                         while (!ffmpeg.HasExited)
    //                         {
    //                             try
    //                             {
    //                                 await Vnc.SendSpeakingAsync();
    //                             }
    //                             catch (InvalidOperationException)
    //                             {
    //                                 ffmpeg.Kill();
    //                                 ytdl.Kill();
    //                                 await me.Channel.SendMessageAsync("Bot disconnected, playback terminated!");
    //                             }
    //
    //                             Thread.Sleep(1000);
    //                         }
    //                     }).Start();
    //
    //                     //while (!ffmpeg.HasExited) txStream.WriteByte((byte)ffout.ReadByte());
    //                     await ffout.CopyToAsync(txStream);
    //                 }
    //
    //                 //await ffout.CopyToAsync(txStream);
    //                 await txStream.FlushAsync();
    //                 //ffmpeg.WaitForExit();
    //                 //while (vnc.IsPlaying) { Thread.Sleep(1000); }
    //                 await Vnc.SendSpeakingAsync(false);
    //
    //                 await me.Channel.SendMessageAsync("Finished playing!");
    //             }
    //         }
    //     }
    // }
    public async void Play(MessageEnvironment me)
    {
        var vne = me.Bot.DiscordClient.GetVoiceNext();
        if (!IsPlaying)
        {
            IsPlaying = true;
            //VoiceNextConnection vnc;
            if ((await me.Channel.Guild.GetMemberAsync(me.Message.Author.Id)).VoiceState == null)
            {
                await me.Channel.SendMessageAsync("You must be connected to a voice channel!");
            }
            else if (vne.GetConnection(me.Channel.Guild) == null)
            {
                Vnc = await vne.ConnectAsync((await me.Channel.Guild.GetMemberAsync(me.Message.Author.Id)).VoiceState.Channel);
                await me.Channel.SendMessageAsync("Connected!");
            }
            
            if (vne.GetConnection(me.Channel.Guild) == null)
            {
                await me.Channel.SendMessageAsync("Not connected in this server! Bug?");
            }
            else
            {
                while (Queue.Queue.Count > 0)
                {
                    currentlyPlaying = Queue.Next();
                    Vnc = vne.GetConnection(me.Channel.Guild);
                    while (Vnc.IsPlaying) Thread.Sleep(100);
                    vts = Vnc.GetTransmitSink();
                    vts.VolumeModifier = 0.75;

                    await me.Channel.SendMessageAsync("Preparing!");
                    var ffout = AudioDecoder.GetPCM(currentlyPlaying.Id);

                    await me.Channel.SendMessageAsync($"Playing *{currentlyPlaying.Name}* (<{currentlyPlaying.Url}>)!");
                    await ffout.CopyToAsync(vts);
                    await vts.FlushAsync();
                    await Vnc.WaitForPlaybackFinishAsync();
                    
                }
                
                await Vnc.SendSpeakingAsync(false);
                await me.Channel.SendMessageAsync("Finished playing!");
                IsPlaying = false;
            }
        }
    }

    public void Pause()
    {
    }

    public void Skip()
    {
    }
}

internal class ServerMusicPlayerQueue
{
    public readonly Queue<MusicItem> Queue = new();

    public async Task<MusicItem> Add(string url, CommandEnvironment? ce)
    {
        var mi = new MusicItem {Url = url, Id = DownloadIfNotExist(url, ce)};
        Queue.Enqueue(mi);
        return mi;
    }

    public MusicItem Next()
    {
        return Queue.Count > 0 ? Queue.Dequeue() : null;
    }

    private string DownloadIfNotExist(string url, CommandEnvironment? statusMessage)
    {
        string id = String.Join("", SHA256.HashData(Encoding.UTF8.GetBytes(url)).Select(x=>x.ToString("x2")));
        if (
            !File.Exists(AudioDecoder.CacheDir + "dl/" + id) &&
            !File.Exists(AudioDecoder.CacheDir + "pcm/" + id + ".wav")
        )
        {
            //var msg = statusMessage.Channel.SendMessageAsync("Downloading...").Result;
            var ytdlpsi = new ProcessStartInfo
            {
                FileName = "youtube-dl",
                Arguments =
                    $@"-ci -f best ""{url}"" -o ""{AudioDecoder.CacheDir + "dl/"}{id}""",
                RedirectStandardOutput = false,
                UseShellExecute = false
            };
            var ytdl = Process.Start(ytdlpsi);
            ytdl.WaitForExit();
            //msg.ModifyAsync("Downloading... Done!\nProcessing...");
            AudioDecoder.GetPCM(id);
        }

        return id;
    }
}

internal class MusicItem
{
    public string Icon = "";
    public string Name = "Unknown";
    public string Url;
    public string Id;
    public List<Byte> OpusData = new();
}