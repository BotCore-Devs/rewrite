namespace BotCore;

public class Version
{
    public static string VersionNum = "4.0.0a1";
    public static string GitVer
    {
        get => VersionNum + "+" + ArcaneLibs.Util.GetCommandOutputSync("git", "rev-parse --short HEAD");
    }
    
}