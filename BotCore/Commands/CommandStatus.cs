using BotCore.Classes;

namespace BotCore.Commands;

public class CommandStatus
{
    //static
    public static List<CommandStatus> CommandStatusList = new();
    public CommandEnvironment CommandEnvironment;
    public DateTime EndTime = DateTime.Now;
    public List<Exception> Exceptions = new();
    public bool Running = true;

    //instance
    public DateTime StartTime = DateTime.Now;

    //ctor
    public CommandStatus(bool running, CommandEnvironment commandEnvironment)
    {
        Running = running;
        CommandEnvironment = commandEnvironment;
        commandEnvironment.CommandStatus = this;

        CommandStatusList.Add(this);
    }

    //calculated properties
    public TimeSpan TimeSpent => (Running ? DateTime.Now : EndTime) - StartTime;
    public bool Errored => Exceptions.Count > 0;

    //functions
    public void MarkDone()
    {
        Running = false;
        EndTime = DateTime.Now;
    }
}