using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using ArcaneLibs;
using BotCore.DbExtras;
using BotCore.Util;
using DSharpPlus.Entities;
using Newtonsoft.Json;
using Z.Expressions;

namespace BotCore.Commands.Commands.Groups;

[SuppressMessage("Interoperability", "CA1416:Validate platform compatibility")]
internal class OwnerCommands : CommandGroup
{
    //all of these commands are restricted to The Arcane Brony
    //run command, runs arbitrary code
    public static Command RunCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "run",
            Category = "Owner",
            Description = "Run a payload",
            action = async (me, ce) =>
            {
                Console.WriteLine(string.Join(", ", ce.Args));
                switch (ce.Args[0])
                {
                    // case "giveallmember":
                    // foreach (DiscordMember mmb in await me.Channel.Guild.GetAllMembersAsync())
                    // {
                    // if (!mmb.IsBot)
                    // {
                    // await mmb.GrantRoleAsync(me.Channel.Guild.GetRole(me.Server.Roles.Member.Value));
                    // }
                    // }

                    // break;
                    case "testing":
                        await me.Channel.SendMessageAsync($@"
Have you ever felt like {bot.Bot.Name} was unstable?
Have you ever wanted to see a little behind the scenes?
Have you ever wanted to help test the bot?
Have you ever wanted to suggest your own features?

Take this invite link straight into the bot development server!

https://discord.gg/VwKMryw");
                        break;
                    // case "invites":
                    // foreach (DiscordGuild guild in ce.Client.Guilds.Values)
                    // {
                    // try
                    // {
                    // await me.Channel.SendMessageAsync("https://discord.gg/" +
                    // (await guild.Channels.Values.First()
                    // .CreateInviteAsync(max_uses: 1, temporary: true))
                    // .Code);
                    // Thread.Sleep(2000);
                    // }
                    // catch
                    // {
                    // await me.Channel.SendMessageAsync($"Failed to create invite for {guild.Name}");
                    // }
                    // }

                    // break;
                    // case "10248647":
                    //     if (Dns.GetHostName() == "DESKTOP-0ABLLGL")
                    //     {
                    //         Random rnd = new Random();
                    //         foreach (DiscordMember member in me.Channel.Guild.Members.Values)
                    //         {
                    //             LegacyUser user =
                    //                 (await bot.DataStore.GetServer(me.Channel.Guild.Id)).GetUser(member.Id);
                    //             user.XpLevel = rnd.Next(40);
                    //             user.Xp = rnd.Next(user.XpGoal);
                    //             user.AvatarUrl = member.AvatarUrl;
                    //             user.Changed = true;
                    //             user.Credits = rnd.Next();
                    //             user.Left = rnd.Next(0, 2) == 1;
                    //             user.MessageCount = rnd.Next();
                    //             user.Username = member.Username;
                    //             user.Discriminator = member.Discriminator;
                    //
                    //             user.Save();
                    //         }
                    //     }
                    //
                    //     break;
                    case "getprunecount":
                        await me.Channel.SendMessageAsync(
                            $"Pruneable members:\n  - 07 days inactivity: {await me.Channel.Guild.GetPruneCountAsync()}\n  - 30 days inactivity: {await me.Channel.Guild.GetPruneCountAsync(30)}");

                        break;
                    case "chlist":
                        await me.Channel.SendMessageAsync(
                            $"Channel list:\n\n- {string.Join("\n- ", (await me.Channel.Guild.GetChannelsAsync()).OrderBy(x=>x.Position).Select(x => x.Name))}");
                        break;
                    default:
                        await me.Channel.SendMessageAsync("Payload not found!");
                        break;
                }

                return null;
            }
        };
    }

    //cmd command, runs a command in cmd and sends the output in one or more messages
    public static Command CmdCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "cmd",
            Category = "Owner",
            Description = "Run a console command",
            action = async (me, ce) =>
            {
                var cmd = "\"";
                foreach (var arg in ce.Args) cmd += arg + " ";

                cmd = cmd.Trim();
                cmd += "\"";

                await me.Channel.SendMessageAsync($"Command being executed: `{cmd}`");

                var output = ArcaneLibs.Util.GetCommandOutputSync(BotEnvironment.IsLinux ? "/bin/sh" : "cmd.exe",
                        (BotEnvironment.IsLinux ? "-c " : "/c ") + cmd)
                    .Replace("`", "\\`")
                    .StripDiscordUnsupported()
                    .Split("\n").ToList();
                foreach (var _out in output) Console.WriteLine($"{_out.Length:0000} {_out}");

                var msg = "```ansi\n";
                while (output.Count > 1)
                {
                    Console.WriteLine("Adding: " + output[0]);
                    msg += output[0] + "\n";
                    output.RemoveAt(0);
                    if (msg.Length > 1997)
                    {
                        output.Insert(0, msg[1997..]);
                        msg = msg[..1997];
                        await me.Channel.SendMessageAsync(msg + "```");
                        msg = "```ansi\n";
                    }
                    else if (msg.Length + output[0].Length > 1997)
                    {
                        await me.Channel.SendMessageAsync(msg + "```");
                        msg = "```ansi\n";
                    }
                }

                Console.WriteLine("Remainder: " + msg);
                await me.Channel.SendMessageAsync(msg + "```");
                return null;
            }
        };
    }

    //modtoggle command, toggles moderation features in current server
    public static Command ModToggleCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "modtoggle",
            Category = "Owner",
            Description = "Toggle moderation features for server",
            action =
                async (me, ce) =>
                {
                    me.Server.ModerationEnabled = !me.Server.ModerationEnabled;
                    await me.Channel.SendMessageAsync(
                        $"Moderation has been {(me.Server.ModerationEnabled ?? false ? "en" : "dis")}abled for this server!");
                    return null;
                }
        };
    }

    //serverlist command, displays a list of servers with invites
    public static Command ServerListCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "serverlist",
            Category = "Owner",
            Description = "List of servers the bot is in",
            action = async (me, ce) =>
            {
                var guilds = "Server list:\n";
                foreach (var (_, guild) in me.Bot.DiscordClient.Guilds)
                {
                    guilds += $"{guild.Name} ({guild.MemberCount} members)\n";
                    guilds += "\n";
                    var state = "Could not create invite...";
                    foreach (var ch in guild.Channels.Values)
                    {
                        try
                        {
                            state =
                                $"{guild.Name}/{ch.Name}: discord.gg/{(await ch.CreateInviteAsync()).Code}\n";
                            break;
                        }
                        finally //does not increase code complexity
                        {
                        }
                    }

                    guilds += state + "\n";
                }

                await me.Channel.SendMessageAsync(guilds);
                return null;
            }
        };
    }
    
    //fake ban
    public static Command BanCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "ban",
            Category = "Owner",
            Description = "",
            action = async (me, ce) =>
            {
                await me.Channel.SendMessageAsync($"Banned {me.Message.MentionedUsers[0]}!");
                return null;
            }
        };
    }

    //joinhist command, shows the evolution of server size (retained members) over time
    public static Command JoinHistCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "joinhist",
            Category = "Owner",
            Description =
                "Server size over time (not including members that left/rejoined)",
            action = async (me, ce) =>
            {
                // string a = "JoinHistory.";
                var gsw = Stopwatch.StartNew();
                // Stopwatch gsw = bot.Timing.StartTiming("JoinHistory"),
                // sw = bot.Timing.StartTiming(a + "GetMembers");
                var msg = await me.Channel.SendMessageAsync("Just give me a minute 👌");
                IReadOnlyCollection<DiscordMember> members = await me.Channel.Guild.GetAllMembersAsync();
                msg = await msg.ModifyAsync($"{msg.Content}\n[{gsw.Elapsed}] Got all {members.Count} members!");
                // sw = bot.Timing.GotoNext(sw, a + "ProcessMembers");
                var dayCount = (int) DateTime.Now.Date.Subtract(me.Channel.Guild.CreationTimestamp.DateTime.Date)
                    .TotalDays;
                Console.WriteLine(dayCount);
                int i;
                /*
                Console.WriteLine("Getting gfx ctx");
                using (var bmp = new Bitmap(dayCount, members.Count))
                {
                    Console.WriteLine("got bpm");
                    using (var g = Graphics.FromImage(bmp))
                    {
                        Console.WriteLine("Got graphics ctx");
                        var pen = new Pen(Color.FromArgb(0xFF, 0xFF, 0x00, 0xFF));
                        var pen2 = new Pen(Color.FromArgb(0xFF, 0xFF, 0x00, 0x00));
                        i = 0;
                        for (var x = 0; x < dayCount; x++)
                        {
                            var b = members.Count(m =>
                                m.JoinedAt.Date == DateTime.Now.Date
                                    .Subtract(new TimeSpan(dayCount - x, 0, 0, 0)).Date);
                            //Console.WriteLine(b);
                            g.DrawLine(pen, x, members.Count, x, members.Count - (i += b));
                            g.DrawLine(pen2, x, members.Count, x, members.Count - b);
                        }

                        /*foreach (DiscordMember member in members.OrderBy(x => x.JoinedAt))
                    {
                        //Console.WriteLine($"{member.JoinedAt}");
                        int x = dayCount - (int)DateTime.Now.Date.Subtract(member.JoinedAt.Date).TotalDays;
                        Console.WriteLine(x);
                        g.DrawLine(pen, x, 0, x, i++);
                    }* /
                        g.Save();
                    }

                    bmp.Save("a.png");
                }*/


                msg = await msg.ModifyAsync(
                    $"{msg.Content}\n[{gsw.Elapsed}] Done generating image for {members.Count} members!");
                var html = $@"<html>
    <head>
        <!-- <meta http-equiv='refresh' content='1'> -->
        <title>Join history for {me.Channel.Guild.Name}</title>
        <style>
            body {{ margin: 0px; background-color: #333; width: max-content; overflow-y: hidden;}}
            tc {{width: 0px; border-right: 1px solid blue; margin: -1px; display: inline-block;}}
            #tooltip {{background-color: #444; height: 100px; width: 250px; position:absolute;}}
            #serverinfo {{background-color: #444; height: 100px; width: 250px; position:absolute;}}
        </style>
        <script src='https://code.jquery.com/jquery-3.5.1.min.js'></script>
    </head>
    <body>
        <div id='tooltip'></div>
        <tc style='height: 10px;'></tc>
        $DAY
    </body>
    <script>
        $('tc').hover(a=>{{console.log(a.target)}});
        $('tc').hover(a=>{{console.log(a.attributes['mc'].value)}});
        $('body').mousemove(a=>{{$('#tooltip').css({{'left':a.pageX+16, 'top':a.pageY+32}})}});
    </script>
</html>";
                i = 0;
                var rnd = new Random();
                for (var x = 0; x < dayCount; x++)
                {
                    var _members = members.Where(m =>
                            m.JoinedAt.Date == DateTime.Now.Date.Subtract(new TimeSpan(dayCount - x, 0, 0, 0))
                                .Date)
                        .ToArray();
                    var b = _members.Length;
                    Dictionary<string, string> _newmembers = new();
                    foreach (var member in _members)
                        _newmembers.Add(member.AvatarUrl + "&a=" + rnd.Next(100000),
                            $"<p style='color: {member.Color.Value};'>{member.Username}#{member.Discriminator}</p>");

                    //g.DrawLine(pen, x, members.Count, x, members.Count - (i += b));
                    //g.DrawLine(pen2, x, members.Count, x, members.Count - b);
                    html = html.Replace("$DAY",
                        $@"<tc style='height: calc(100% * ({i += b} / {members.Count}));' d='{DateTime.Now.Date.Subtract(new TimeSpan(dayCount - x, 0, 0, 0)).Date}' mc='{i}'><a style='display:none;'>{JsonConvert.SerializeObject(_newmembers)}</a></tc>
        $DAY");
                }

                html = html.Replace("$DAY", $"<div id='membercount' value='{members.Count}'></div>");
                File.WriteAllText("test.html", html);
                msg = await msg.ModifyAsync(
                    $"{msg.Content}\n[{gsw.Elapsed}] Done generating HTML for {members.Count} members!");
                // bot.Timing.StopTiming(sw);
                //await msg.DeleteAsync();
                //var fs = File.OpenRead("a.png");
                var fs2 = File.OpenRead("test.html");
                //await me.Channel.SendMessageAsync(builder => builder.WithFile(fs).WithContent(
                // $"{msg.Content}\nServer growth of remaining member since {me.Channel.Guild.CreationTimestamp.Date}"));
                await me.Channel.SendMessageAsync(builder => builder.WithFile(fs2));
                //fs.Close();
                fs2.Close();

                // bot.Timing.StopTiming(gsw);
                gsw.Stop();
                return null;
            }
        };
    }

    public static Command EvalCommandDefinition(BotImplementation bot)
    {
        return new()
        {
            Name = "eval",
            Category = "Owner",
            Description = "Evaluate C# code",
            action = async (me, ce) =>
            {
                try
                {
                    var code = me.Message.Content.Replace(me.Server.Prefix + "eval ", "");
                    var i = code.IndexOf("\n", StringComparison.Ordinal);
                    code = code[i..];
                    i = code.LastIndexOf("```", StringComparison.Ordinal);
                    code = code.Substring(0, i);

                    try
                    {
                        await me.Channel.SendMessageAsync(
                            $"Code output (Z.Expressions.Eval):\n\n{Eval.Execute(code, new {bot, me, ce})}");
                    }
                    catch (Exception e)
                    {
                        await me.Channel.SendMessageAsync(
                            $"Something has gone wrong whilst executing your code (Z.Expressions.Eval)!```cs\n{e}```");
                    }
                }
                catch (Exception e)
                {
                    await me.Channel.SendMessageAsync(
                        $"Something has gone wrong whilst executing your code!```cs\n{e}```");
                }

                return null;
            }
        };
    }

    //set custom slowmode
    public static Command SetCSlowCommandDefinition(BotImplementation bot)
    {
        return new Command()
        {
            Name = "setcslow",
            Category = "Owner",
            Description = "Set custom slowmode time",
            action = async (me, ce) =>
            {
                me.Channel.SendMessageAsync($"Parsed time: {ArcaneLibs.Util.ParseTime(ce.Args[0])} ({TimeSpan.FromSeconds(ArcaneLibs.Util.ParseTime(ce.Args[0]))})");
                var sc = me.Db.SlowmodeConfigs.SingleOrDefault(x => x.DiscordChannelId == me.Channel.Id);
                if (sc is null)
                {
                    me.Db.SlowmodeConfigs.Add(sc = new()
                    {
                        Server = me.Server,
                        DiscordChannelId = me.Channel.Id
                    });
                }

                sc.SlowmodeTime = TimeSpan.FromSeconds(ArcaneLibs.Util.ParseTime(ce.Args[0]));
                await me.Db.SaveChangesAsync();
                return null;
            }
        };
    }
    //roleinfo command
    public static Command RoleInfoCommand(BotImplementation bot)
    {
        return new Command()
        {
            Name = "roleinfo",
            Category = "Owner",
            Description = "Get role info",
            action = async (me, ce) =>
            {
                var role = me.Message.Channel.Guild.GetRole(ulong.Parse(ce.Args[0]));
                await me.Channel.SendMessageAsync(
                    $"```json\n{JsonConvert.SerializeObject(role, Formatting.Indented)}```");
                return null;
            }
        };
    }
}