﻿namespace BotCore.Commands.Commands.Groups;

public class TicketCommands : CommandGroup
{
#if false
            //ticket command, handles all ticket operations, such as opening, and closing tickets
            public static CommandDefinition TicketCommandDefinition(BotImplementation bot)=>
                new CommandDefinition("ticket", CommandCategory.Ticket,
                    "Open/manage tickets", async (ce) =>
                    {
                        switch (ce.Args.Count>0?ce.Args[0]:"")
                        {
                            case "open":
                                if (me.Server.Tickets.Any(x => x.OwnerId == me.Message.Author.Id && x.IsOpen))
                                {
                                    await me.Channel.SendMessageAsync("You already own a ticket in this server! You must close it first!");
                                } else
                                {
                                    DiscordChannel newlyOpenedChannel =
                                        (await me.Channel.Guild.CreateChannelAsync(
                                            $"{me.Server.Tickets.Count}-{me.Message.Author.Username.ToAlphaNumeric()}",
                                            ChannelType.Text, me.Channel.Guild.Channels[747981696884015147]));
                                    me.Server.Tickets.Add(new Ticket()
                                    {
                                        OwnerId = me.Message.Author.Id,
                                        ChannelId = newlyOpenedChannel.Id
                                    });
                                    await me.Channel.SendMessageAsync(
                                        $"New ticket opened! {newlyOpenedChannel.Mention}");
                                }

                                me.Server.Save();
                                break;
                            case "close":
                                Ticket ticketToClose =
 me.Server.Tickets.FirstOrDefault(x => x.OwnerId == me.Message.Author.Id && x.IsOpen);
                                if (ticketToClose == null)
                                {
                                    await me.Channel.SendMessageAsync("You don't have any open tickets!");
                                }
                                else
                                {
                                    
                                    me.Server.Tickets[me.Server.Tickets.IndexOf(ticketToClose)].IsOpen = false;
                                    ticketToClose.IsOpen = false;
                                    me.Server.Save();
                                    Console.WriteLine(ticketToClose.IsOpen);
                                    DiscordChannel channelToClose = me.Channel.Guild.Channels[ticketToClose.ChannelId];
                                    await channelToClose.ModifyAsync((model =>
                                    {
                                        model.Name = 'c' + channelToClose.Name;
                                        model.AuditLogReason = "Closed ticket";
                                    }));
                                    await channelToClose.AddOverwriteAsync(me.Channel.Guild.EveryoneRole,
                                        deny: Permissions.AccessChannels, reason: "Closed ticket");
                                    
                                }
                                break;
                            case "delete":
                                if (!Checks.IsStaff(ce))
                                {
                                    await me.Channel.SendMessageAsync("You must be staff to use this command!");
                                    return null;
                                }
                                if (ce.Args.Count == 1)
                                {
                                    await me.Channel.SendMessageAsync("You must provide a channel ID");
                                    return null;
                                }
                                Ticket ticketToDelete =
 me.Server.Tickets.FirstOrDefault(x => x.ChannelId + "" == ce.Args[1]);
                                if (ticketToDelete == null)
                                {
                                    await me.Channel.SendMessageAsync("Ticket doesnt exist??");
                                }
                                else
                                {
                                    me.Server.Tickets.Remove(ticketToDelete);
                                    me.Server.Save();
                                    DiscordChannel channelToDelete = me.Channel.Guild.Channels[ticketToDelete.ChannelId];
                                    await channelToDelete.DeleteAsync("Ticket deleted: " + channelToDelete.Name);
                                    
                                }
                                break;
                            default:
                                await me.Channel.SendMessageAsync("Ticket system:\n\nAvailable subcommands:\n- open\n- close\n- delete <channel id> (admin only)");
                                break;
                        }
                    });

#endif
}