﻿using BotCore.DbExtras;
using BotCore.Util.Extensions.DSharpPlus;
using DSharpPlus;
using DSharpPlus.Entities;
using DSharpPlus.Exceptions;

namespace BotCore.Commands.Commands.Groups;

internal class ModCommands : CommandGroup
{
    //config command, lets you manage settings for the bot, barely contains any settings however.
    // public static CommandDefinition ConfigCommandDefinition(BotImplementation bot)=> new CommandDefinition("config",
    //     Category="Moderation", Description = "Configure settings for the server")
    // {
    //     Action = async (ce) =>
    //     {
    //         if (!me.Message.Channel.PermissionsFor(((DiscordMember) me.Message.Author))
    //             .HasFlag(Permissions.ManageGuild))
    //         {
    //             await me.Channel.SendMessageAsync("You need `Manage Server` to use this command!");
    //             return null;
    //         }
    //
    //         if (ce.Args.Count != 0)
    //         {
    //             await HandleConfigSubcommands(ce);
    //         }
    //         else
    //         {
    //             await me.Channel.SendMessageAsync(
    //                 "Subcommands: prefix, autorole, wordfilter, namefilter, ignoredchannels, contentfilter");
    //         }
    //     },
    //     PermissionCheck = ce => me.Message.Channel.PermissionsFor(((DiscordMember) me.Message.Author))
    //         .HasFlag(Permissions.ManageGuild)
    // };

    //mute command, mutes a user for a period of time if provided
    public static Command MuteCommandDefinition(BotImplementation bot)
    {
        return new Command()
        {
            Name = "mute",
            Category = "Moderation",
            Description = "Mute a user for a set amount of time",
            action = async (me, ce) =>
            {
                //code
                // if (false && !Checks.UserAffectableByModeration(ce))
                // {
                // await me.Channel.SendMessageAsync("This user can't be muted, due to safety restrictions.");
                // return null;
                // }

                if (ce.Args.Count == 1)
                {
                    if (me.Message.Channel.PermissionsFor((DiscordMember) me.Message.MentionedUsers[0])
                        .HasFlag(Permissions.ManageMessages))
                    {
                        if (((DiscordMember) me.Message.MentionedUsers[0]).Hierarchy <
                            ((DiscordMember) me.Message.Author).Hierarchy)
                            //me.Channel.Guild.GetRole(589673360242376716)
                            try
                            {
                                await ((DiscordMember) me.Message.MentionedUsers[0]).GrantRoleAsync(me.Channel.Guild
                                    .Roles
                                    .First(x => x.Value.Name == "Muted").Value);
                                me.MentionedDSUser.Muted = true;
                                await me.Channel.SendMessageAsync("User has been successfully muted!"); // success
                                
                                // Log.Mod($"{me.Message.Author.DisplayName()} has muted {me.Message.MentionedUsers[0].DisplayName()} indefinitely.");
                            }
                            catch (InvalidOperationException)
                            {
                                await me.Channel.SendMessageAsync("No `Muted` role found!");
                            }
                            catch (UnauthorizedException)
                            {
                                await me.Channel.SendMessageAsync("Bot does not have permission to manage roles!");
                            }
                    }
                    else
                    {
                        await me.Channel.SendMessageAsync("You do not have permission to use this command!");
                    }
                }
                else if (ce.Args.Count >= 2)
                {
                    ulong time;
                    if ((time = ArcaneLibs.Util.ParseTime(ce.Args[1])) > 0)
                        me.MentionedDSUser.Muted = true;
                    // me.MentionedDSUser.Mute();
                    // await me.Channel.SendMessageAsync(
                    //     $"User has been successfully muted until {me.MentionedDSUser.UnmuteBy}!"); // success
                    //Log.Mod($"{me.Message.Author.DisplayName()} has muted {me.Message.MentionedUsers[0].DisplayName()} until {me.MentionedDSUser.UnmuteBy}!");
                    else
                        await me.Channel.SendMessageAsync(
                            $"You have entered an invalid time string! ({time} seconds)"); // success
                }
                else
                {
                    await me.Channel.SendMessageAsync("You must ping a member!"); // no member me.MentionedDSUser
                }

                return null;
            },
            CanRun = async (me, ce) => me.Message.Channel.PermissionsFor(await me.Message.GetAuthorAsMemberAsync())
                .HasFlag(Permissions.ManageMessages)
        };
    }

    //unmute command, unmutes a user
    public static Command UnmuteCommandDefinition(BotImplementation bot)
    {
        return new Command()
        {
            Name = "unmute",
            Category = "Moderation",
            Description = "Unmute user",
            action = async (me, ce) =>
            {
                if (!me.Message.Channel.PermissionsFor((DiscordMember) me.Message.Author)
                        .HasFlag(Permissions.ManageMessages))
                {
                    await me.Channel.SendMessageAsync("You need `Manage Messages` to use this command!");
                    return null;
                }

                //code
                if (ce.Args.Count >= 1)
                {
                    // me.MentionedDSUser.Unmute();
                    // await me.Channel.SendMessageAsync("User has been successfully unmuted!");
                }
                else
                {
                    await me.Channel.SendMessageAsync("You must ping a member!");
                }

                return null;
            },
            CanRun = async (me, ce) => me.Message.Channel.PermissionsFor(await me.Message.GetAuthorAsMemberAsync())
                .HasFlag(Permissions.ManageMessages)
        };
    }

    //slowmode command, set channel slowmode
    public static Command SlowmodeCommandDefinition(BotImplementation bot)
    {
        return new Command()
        {
            Name = "slowmode",
            Category = "Moderation",
            Description = "Set a custom slowmode time",
            action = async (me, ce) =>
            {
                if (!me.Message.Channel.PermissionsFor((DiscordMember) me.Message.Author)
                        .HasFlag(Permissions.ManageChannels))
                {
                    await me.Channel.SendMessageAsync("You need `Manage Channel` to use this command!");
                    return null;
                }

                var seconds = ArcaneLibs.Util.ParseTime(ce.Args[0]);
                if (!(seconds > 0 || seconds < 21600))
                {
                    await me.Channel.SendMessageAsync(
                        $"Invalid number entered, you need to enter the time in seconds! (0 - 21600s/6h)\nYou entered: `{ce.Args[0]}` and it evaluated to {seconds} seconds."); // invalid number
                    return null;
                }

                await me.Message.Channel.ModifyAsync(em =>
                {
                    em.PerUserRateLimit = (int) seconds;
                    em.AuditLogReason =
                        $"Rate limit set to {seconds} by {me.Message.Author.Username}#{me.Message.Author.Discriminator}.";
                });
                await me.Channel.SendMessageAsync($"Set channel slowmode to {seconds} seconds!");
                return null;
            },
            CanRun = async (me, ce) => me.Message.Channel.PermissionsFor(await me.Message.GetAuthorAsMemberAsync())
                .HasFlag(Permissions.ManageChannels)
        };
    }

    //prune command, removes an amount of messages
    public static Command PruneCommandDefinition(BotImplementation bot)
    {
        return new Command()
        {
            Name = "prune",
            Category = "Moderation",
            Description = "Prune an amount of messages",
            action = async (me, ce) =>
            {
                if (!me.Message.Channel.PermissionsFor((DiscordMember) me.Message.Author)
                        .HasFlag(Permissions.ManageMessages))
                {
                    await me.Channel.SendMessageAsync("You need `Manage Messages` to use this command!");
                    return null;
                }

                if (!int.TryParse(ce.Args[0], out var count))
                {
                    await me.Channel.SendMessageAsync(
                        "Invalid number entered, you need to enter the number of messages!"); // invalid number
                    return null;
                }

                await me.Message.Channel.DeleteMessagesAsync(await me.Message.Channel.GetMessagesAsync(count + 1),
                    $"{count} messages pruned by {me.Message.Author.Username}#{me.Message.Author.Discriminator}");
                await me.Channel.SendMessageAsync($"Pruned {count} messages!");
                return null;
            },
            CanRun = async (me, ce) => me.Message.Channel.PermissionsFor(await me.Message.GetAuthorAsMemberAsync())
                .HasFlag(Permissions.ManageMessages)
        };
    }

    //warn command, adds a warning to a user
    public static Command CommandDefinition(BotImplementation bot)
    {
        return new Command()
        {
            Name = "warn",
            Category = "Moderation",
            Description = "Warn a user",
            action = async (me, ce) =>
            {
                if (!me.Message.Channel.PermissionsFor((DiscordMember) me.Message.Author)
                        .HasFlag(Permissions.ManageMessages))
                {
                    await me.Channel.SendMessageAsync("You need `Manage Messages` to use this command!");
                    return null;
                }

                if (ce.Args.Count >= 2)
                    ce.Args.RemoveAt(0);
                // me.MentionedDSUser.Warnings.Add(string.Join(" ", ce.Args));
                // await me.Channel.SendMessageAsync(
                // $"Warned {me.Channel.Guild.Members[me.Message.MentionedUsers[0].Id].DisplayName()} (#{me.MentionedDSUser.Warnings.Count}: {string.Join(" ", ce.Args)})");
                else
                    await me.Channel.SendMessageAsync("You must enter a reason");

                return null;
            },
            CanRun = async (me, ce) => me.Message.Channel.PermissionsFor(await me.Message.GetAuthorAsMemberAsync())
                .HasFlag(Permissions.ManageMessages)
        };
    }

    //disablecmd command, disables a command in current server
    public static Command DisableCmdCommandDefinition(BotImplementation bot)
    {
        return new Command()
        {
            Name = "disablecmd",
            Category = "Moderation",
            Description = "Disable a command",
            action = async (me, ce) =>
            {
                if (!me.Message.Channel.PermissionsFor((DiscordMember) me.Message.Author)
                        .HasFlag(Permissions.ManageGuild))
                {
                    await me.Channel.SendMessageAsync("You need `Manage Server` to use this command!");
                    return null;
                }

                if (ce.Args.Count >= 1)
                    foreach (var cmd in ce.Args)
                        me.Server.DisabledCommands.Add(cmd);
                else
                    await me.Channel.SendMessageAsync("You must enter a command");

                return null;
            },
            CanRun = async (me, ce) => me.Message.Channel.PermissionsFor(await me.Message.GetAuthorAsMemberAsync())
                .HasFlag(Permissions.ManageGuild)
        };
    }

    //warns command, displays all warnings for a user
    public static Command WarnsCommandDefinition(BotImplementation bot)
    {
        return new Command()
        {
            Name = "warns",
            Category = "Moderation",
            Description = "List the warnings for a user",
            action = async (me, ce) =>
            {
                await me.Channel.SendMessageAsync(
                    $"Warns for {me.Message.MentionedUsers[0]}:\n```  - {string.Join(".\n  - ", me.MentionedDSUser.Warnings)}```"); // no member me.MentionedDSUser
                return null;
            }
        };
    }

    //removewarn command, removes a warning from a user
    public static Command RemoveWarnCommandDefinition(BotImplementation bot)
    {
        return new Command()
        {
            Name = "removewarn",
            Category = "Moderation",
            Description = "Remove a warning from a user",
            action = async (me, ce) =>
            {
                if (!me.Message.Channel.PermissionsFor((DiscordMember) me.Message.Author)
                        .HasFlag(Permissions.ManageMessages))
                {
                    await me.Channel.SendMessageAsync("You need `Manage Messages` to use this command!");
                    return null;
                }

                if (int.TryParse(ce.Args[1], out var num))
                {
                    // me.MentionedDSUser.Warnings.Remove(me.MentionedDSUser.Warnings.);
                    // await me.Channel.SendMessageAsync($"Warning removed!");
                }
                else
                {
                    await me.Channel.SendMessageAsync("Enter a correct number!");
                }

                return null;
            },
            CanRun = async (me, ce) => me.Message.Channel.PermissionsFor(await me.Message.GetAuthorAsMemberAsync())
                .HasFlag(Permissions.ManageMessages)
        };
    }

    // static async Task HandleConfigSubcommands(CommandEnvironment ce)
    // {
    //     switch (ce.Args[0])
    //     {
    //         case "levels":
    //             await me.Channel.SendMessageAsync($"Leveling has been {me.Server.LevelsEnabled.Toggle()}!");
    //             break;
    //         case "prefix":
    //             if (ce.Args.Count == 1)
    //             {
    //                 await me.Channel.SendMessageAsync(
    //                     $"Prefix changed: `{me.Server.Prefix}` > `{me.Server.Prefix = ce.Args[1]}`.");
    //             }
    //             else
    //             {
    //                 await me.Channel.SendMessageAsync($"Current prefix: `{me.Server.Prefix}`.");
    //             }
    //
    //             break;
    //         case "autorole":
    //             await me.Channel.SendMessageAsync($"Autorole is now {me.Server.AutoRole.Toggle()}abled!");
    //             break;
    //         case "lockdown":
    //             me.Server.LockdownEnabled = !me.Server.LockdownEnabled;
    //             //Log.Mod($"Lockdown mode has been {(config.Lockdown ? "en" : "dis")}abled by {me.Message.Author.DisplayName()}");
    //             await me.Channel.SendMessageAsync($"Lockdown mode has been {(me.Server.Lockdown ? "en" : "dis")}abled!");
    //             break;
    //         // case "contentfilter":
    //         //     switch (ce.Args.Count > 1 ? ce.Args[1] : "get")
    //         //     {
    //         //         case "toggle":
    //         //             me.Server.ContentFilterEnabled = !me.Server.ContentFilterEnabled;
    //         //             await me.Channel.SendMessageAsync(
    //         //                 $"Content filter has been {(me.Server.ContentFilterEnabled ? "en" : "dis")}abled!");
    //         //             break;
    //         //         case "set":
    //         //             if (ce.Args.Count > 3)
    //         //             {
    //         //                 PropertyInfo property = me.Server.ContentFilter.GetType().GetProperty(ce.Args[2]);
    //         //                 if (property == null)
    //         //                 {
    //         //                     await me.Channel.SendMessageAsync(
    //         //                         $"Could not find property (case sensitive!).\nCurrent config:```json\n{JsonConvert.SerializeObject(me.Server.ContentFilter, Formatting.Indented)}```");
    //         //                 }
    //         //                 else
    //         //                 {
    //         //                     switch (property.GetValue(me.Server.ContentFilter))
    //         //                     {
    //         //                         case bool:
    //         //                             property.SetValue(me.Server.ContentFilter, bool.Parse(ce.Args[3]));
    //         //                             break;
    //         //                         case int:
    //         //                             property.SetValue(me.Server.ContentFilter, int.Parse(ce.Args[3]));
    //         //                             break;
    //         //                     }
    //         //                 }
    //         //             }
    //         //
    //         //             break;
    //         //         case "get":
    //         //             await me.Channel.SendMessageAsync(
    //         //                 $"Current config:```json\n{JsonConvert.SerializeObject(me.Server.ContentFilter, Formatting.Indented)}```");
    //         //             break;
    //         //     }
    //         //
    //         //     break;
    //         case "wordfilter":
    //             if (ce.Args.Count == 1)
    //             {
    //                 await me.Channel.SendMessageAsync($"Banned words:\n - {string.Join("\n - ", me.Server.WordFilter)}");
    //             }
    //             else if (ce.Args.Count == 2)
    //             {
    //             }
    //             else
    //             {
    //                 string str = string.Join(" ", ce.Args.GetRange(2, ce.Args.Count - 2));
    //                 switch (ce.Args[1])
    //                 {
    //                     case "add":
    //
    //                         me.Server.WordFilter.Add(str);
    //                         await me.Channel.SendMessageAsync($"Added `{str}` to word filter!");
    //                         break;
    //                     case "remove":
    //                         me.Server.WordFilter.Remove(str);
    //                         await me.Channel.SendMessageAsync($"Removed `{str}` from word filter!");
    //                         break;
    //                 }
    //             }
    //
    //             break;
    //         case "namefilter":
    //             if (ce.Args.Count == 1)
    //             {
    //                 await me.Channel.SendMessageAsync(
    //                     $"Banned name parts:\n - {string.Join("\n - ", me.Server.BannedNameParts)}");
    //             }
    //             else if (ce.Args.Count == 2)
    //             {
    //             }
    //             else
    //             {
    //                 string str = string.Join(" ", ce.Args.GetRange(2, ce.Args.Count - 2));
    //                 switch (ce.Args[1])
    //                 {
    //                     case "add":
    //                         me.Server.BannedNameParts.Add(str);
    //                         await me.Channel.SendMessageAsync($"Added `{str}` to name filter!");
    //                         break;
    //                     case "remove":
    //                         me.Server.BannedNameParts.Remove(str);
    //                         await me.Channel.SendMessageAsync($"Removed `{str}` from name filter!");
    //                         break;
    //                     default:
    //                         await me.Channel.SendMessageAsync($"Unrecognised subcommand *{ce.Args[1]}*!");
    //                         break;
    //                 }
    //             }
    //
    //             break;
    //         case "ignoredchannels":
    //             if (ce.Args.Count == 1)
    //             {
    //                 await me.Channel.SendMessageAsync(
    //                     $"IgnoredChannels:\n - {string.Join("\n - ", me.Server.IgnoredChannels)}");
    //             }
    //             else if (ce.Args.Count == 2)
    //             {
    //             }
    //             else
    //             {
    //                 //string str = string.Join(" ", ce.Args.GetRange(2, ce.Args.Count - 2));
    //                 switch (ce.Args[1])
    //                 {
    //                     case "add":
    //                         foreach (DiscordChannel ch in ce.E.Message.MentionedChannels)
    //                         {
    //                             me.Server.IgnoredChannels.Add(ch.Id);
    //                         }
    //
    //                         await me.Channel.SendMessageAsync($"Added to ignored channel list!");
    //                         break;
    //                     case "remove":
    //                         foreach (DiscordChannel ch in ce.E.Message.MentionedChannels)
    //                         {
    //                             if (me.Server.IgnoredChannels.Contains(ch.Id))
    //                             {
    //                                 me.Server.IgnoredChannels.Remove(ch.Id);
    //                             }
    //                         }
    //
    //                         await me.Channel.SendMessageAsync($"Removed from ignored channel list!");
    //                         break;
    //                     default:
    //                         await me.Channel.SendMessageAsync($"Unrecognised subcommand *{ce.Args[1]}*!");
    //                         break;
    //                 }
    //
    //                 me.Server.IgnoredChannels = me.Server.IgnoredChannels.Distinct().ToList();
    //             }
    //
    //             break;
    //     }
    // }
}