using BotCore.Classes;

namespace BotCore.Commands;

public class Command
{
    public Func<MessageEnvironment, CommandEnvironment, Task<object?>> action = async (me, ce) =>
    {
        await me.Channel.SendMessageAsync("This command hasn't been implemented!");
        return null;
    };

    public Func<MessageEnvironment, CommandEnvironment, Task<bool>> CanRun = async (_, _) => { return true; };
    public string Description = "No description set...";
    public string Name = null!;
    public string Category { get; set; } = "Unknown";
    public bool Donator { get; set; } = false;

    public void Execute(MessageEnvironment me, CommandEnvironment ce)
    {
        //check if user has permission to execute
        if (!HasPermission(me, ce).Result)
        {
            Console.WriteLine("command exec: !hasperms");
            return;
        }

        //check if command can be executed in context
        if (!CanRun.Invoke(me, ce).Result)
        {
            Console.WriteLine("command exec: !canrun");
            return;
        }

        var cs = new CommandStatus(true, ce);
        try
        {
            //try running
            action.Invoke(me, ce);
        }
        catch (Exception e)
        {
            //catch errors
            cs.Exceptions.Add(e);
        }

        cs.MarkDone();
    }

    public async Task<bool> HasPermission(MessageEnvironment me, CommandEnvironment ce)
    {
        //no owner or hidden commands outside of owner(s)
        if ((Category == "Owner" || Category == "Hidden") && me.Message.Author.Id != 84022289024159744)
        {
            if (me.Bot.Bot.Name == "omnibot" && me.Message.Author.Id == 758019943748075602) return true;
            return false;
        }

        if (Category == "Moderation") return CanRun.Invoke(me, ce).Result && (me.Server.ModerationEnabled ?? false);

        return true;
    }
}