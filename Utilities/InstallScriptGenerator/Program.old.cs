﻿/*using System.Linq;
using BotCore.DataModel;

var db = Db.GetPostgres();
while (!File.Exists("DiscordBots.sln"))
{
    Environment.CurrentDirectory += "/..";
    Console.Error.WriteLine($"Scanning for root dir... {Environment.CurrentDirectory}");
}

string startAfter = "";

Console.WriteLine(GetHeader());
Console.Error.WriteLine("Stopping and deleting old services...");
foreach (var svc in Directory.GetFiles("/etc/systemd/system").Select(x => new FileInfo(x).Name)
             .Where(x => x.StartsWith("botcore")))
{
    Console.WriteLine("systemctl disable --now " + svc);
    File.Delete("/etc/systemd/system/" + svc);
}

Console.WriteLine("systemctl daemon-reload");

Console.Error.WriteLine("Creating core services...");
CreateSystemdService("botcore.web", "BotCore Web Interface", "/BotCore.Web.Legacy");
CreateSystemdService("botcore.dataupdater", "BotCore Data Updater", "/BotCore.DataUpdater");

if (args.Contains("--enable")) Console.WriteLine("systemctl enable botcore.web");
if (args.Contains("--enable")) Console.WriteLine("systemctl enable botcore.dataupdater");
if (args.Contains("--start")) Console.WriteLine("systemctl start botcore.web");
if (args.Contains("--start")) Console.WriteLine("systemctl start botcore.dataupdater");

Console.Error.WriteLine("Creating bot services...");
foreach (var dir in Directory.GetDirectories("Bots"))
{
    var s = dir.Replace("Bots/", "");
    if (s == "Legacy" || s.Contains("Test") || s.Contains("Temp")) continue;
    var sn = "botcore." + s.ToLower().Replace("bots.", "bot.");
    Console.Error.WriteLine("Bot directory: " + dir);
    var bot = db.Bots.FirstOrDefault(x => x.Name == sn.Replace("botcore.bot.", ""));
    if ((bot?.Enabled ?? false) && !(bot?.RemotelyDeployed ?? false))
        CreateSystemdService(sn, "BotCore based bot: " + s, "/Bots/" + s);
    startAfter = sn;
}

Console.Error.WriteLine("Building bots...");
Console.WriteLine("dotnet build");
Console.Error.WriteLine("Reloading systemd");
Console.WriteLine("systemctl daemon-reload");
Console.Error.WriteLine("Starting services...");
foreach (var svc in Directory.GetFiles("/etc/systemd/system").Select(x => new FileInfo(x).Name)
             .Where(x => x.StartsWith("botcore.bot.")))
{
    if (args.Contains("--enable")) Console.WriteLine($"systemctl enable {svc}");
    if (args.Contains("--start")) Console.WriteLine($"systemctl start {svc}");
}

GetNginx();
Console.Error.WriteLine("Services:");
Console.Error.WriteLine(string.Join("\n",
    Directory.GetFiles("/etc/systemd/system").Where(x => x.StartsWith("botcore."))
        .Select(x => new FileInfo(x).Name.Replace(".service", ""))));


string GetHeader()
{
    return @"#!/bin/sh
if [ ""$EUID"" -ne 0 ]
  then echo ""Please run as root, also make sure root can read/write these files and folders!""
  exit
fi
if [ ! -f ""DiscordBots.sln"" ]; then
  echo ""You must run this script from the project directory!""
  exit
fi";
}

void CreateSystemdService(string name, string description, string runpath)
{
    Console.Error.WriteLine($"Writing service file: {name}...");
    var content = $@"[Unit]
Description={description}

[Service]
User=root
WorkingDirectory={Environment.CurrentDirectory + runpath}
ExecStart=/bin/dotnet run --nobuild
Restart=always
Type=notify
NotifyAccess=all

[Install]
WantedBy=multi-user.target
{(startAfter.Length >= 2 ? $"After={startAfter}" : "")}";
    if (name == "botcore.dataupdater")
        content = content.Replace("Restart=always", "Restart=always\nRestartSec=600");

    File.WriteAllText($"/etc/systemd/system/{name}.service", content);
}

void GetNginx()
{
    Console.Error.WriteLine(
        @"NGINX config for the web service.
You can add multiple server_name lines if needed, the web service automatically detects which bot you're using:

server {
    server_name botname.domain.tld;
    listen 80;
    listen 443 ssl http2;
    #your ssl/other setup here here
    location / {
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection ""upgrade"";
        proxy_pass_request_headers on;
        proxy_hide_header Access-Control-Allow-Origin;
        add_header Access-Control-Allow-Origin *;
        proxy_ssl_server_name on;
        proxy_cache_key $scheme://$host$request_uri;
        proxy_pass http://localhost:5033;
    }
}");
}*/