#!/bin/sh
for f in *; do
    if [ -d "$f" ]; then
        cd $f
	tabnuget-push
	cd ..
    fi
done
